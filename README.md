# gui-test
## Description

For a GUI Application to run from a docker container, we need to have access to XServer.

XServer is available as part of every Linux Desktop Environment.

However, within a Container we typically don’t have access to interact with the Host Environment.

To accomplish this, we must share the host’s XServer with the container.

## Steps Required

Creating a volume:

- `--volume="$HOME/.Xauthority:/root/.Xauthority:rw"`

Sharing the Host’s `DISPLAY` environment variable to the Container:

- `--env="DISPLAY"`

Running the container with the host network driver.

- `--net=host`

## Example

Use this Dockerfile to build an image with a sample GUI application for testing,

```
FROM debian
RUN apt-get update
RUN apt-get install -y x11-apps
CMD ["/usr/bin/xeyes"]
```

Build the image:

- `$ docker build -t gui-app .`

If we use the steps from above, we can run the container like this:

- `$ docker run --net=host --env="DISPLAY" --volume="$HOME/.Xauthority:/root/.Xauthority:rw" gui-app`

You most likely will get the following error:

```
Authorization required, but no authorization protocol specified
Error: Can't open display: :0
```

This is due to access controls. There are default settings in place to control who can access X-Windows on your host machine.

The docker container process is running as the root user.

The following command will allow the root user on the local machine to connect to the X windows display:

- `$ xhost xhost SI:localuser:root`

You should see the following response:

```
localuser:root being added to access control list
```

If you type, `$ xhost` you should see `SI:localuser:root` added to the list.

Now re-run the docker command from above to test running the gui app:

- `$ docker run --net=host --env="DISPLAY" --volume="$HOME/.Xauthority:/root/.Xauthority:rw" gui-app`

If done correctly, you should see a pair of eyes following your mouse!

## References

- https://medium.com/@SaravSun/running-gui-applications-inside-docker-containers-83d65c0db110
- https://askubuntu.com/questions/990823/apt-gives-unstable-cli-interface-warning 
- https://packages.debian.org/stretch/x11-apps
- https://www.reddit.com/r/linux4noobs/comments/lu1plx/hi_i_get_this_authorization_required_but_no/
- https://stackoverflow.com/questions/48833451/no-protocol-specified-when-running-a-sudo-su-app-on-ubuntu-linux
- https://www.computerhope.com/unix/xhost.htm
- https://www.ibm.com/docs/en/aix/7.2?topic=x-xhost-command
- https://stackoverflow.com/questions/43015536/xhost-command-for-docker-gui-apps-eclipse
